package pl.jgniewowski.zadaniesonalake.controller.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.jgniewowski.zadaniesonalake.service.CalculatorService;
import pl.jgniewowski.zadaniesonalake.controller.dto.CalculationResponse;
import pl.jgniewowski.zadaniesonalake.exceptions.NoRateDataException;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by jarek on 22.03.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CalculatorService calculatorService;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new CalculatorController(calculatorService)).build();
    }

    @Test
    public void shouldPerformCalculationForPLNand1() throws Exception {
        given(this.calculatorService.calculate("PL", "1")).willReturn(new CalculationResponse("1.00"));
        this.mockMvc.perform(post("/calculate").contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"countryCode\": \"PL\", \"dailyWage\": \"1\"}"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount").value("1.00"));
    }

    @Test
    public void shouldPerformCalculationForPLNand1dot00() throws Exception {
        given(this.calculatorService.calculate("PL", "1.00")).willReturn(new CalculationResponse("1.00"));
        this.mockMvc.perform(post("/calculate").contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"countryCode\": \"PL\", \"dailyWage\": \"1.00\"}")).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount").value("1.00"));
    }

    @Test
    public void shouldReturnBadRequestForUnknownCountry() throws Exception {
        given(this.calculatorService.calculate("QA", "100")).willThrow(new UnknownCountryException("QA"));
        this.mockMvc.perform(post("/calculate").contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"countryCode\": \"QA\", \"dailyWage\": \"100\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnBadRequestForWrongAmount() throws Exception {
        given(this.calculatorService.calculate("PL", "x,a0")).willThrow(new NumberFormatException());
        this.mockMvc.perform(post("/calculate").contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"countryCode\": \"PL\", \"dailyWage\": \"x,a0\"}"))
                .andExpect(status().isBadRequest());
    }



}