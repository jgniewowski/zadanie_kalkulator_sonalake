package pl.jgniewowski.zadaniesonalake.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.jgniewowski.zadaniesonalake.controller.dto.CalculationResponse;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;
import pl.jgniewowski.zadaniesonalake.model.CountryParams;
import pl.jgniewowski.zadaniesonalake.model.CurrencyExchangeRate;
import pl.jgniewowski.zadaniesonalake.providers.CountryParamsProvider;
import pl.jgniewowski.zadaniesonalake.providers.ExchangeRateProvider;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

/**
 * Created by jarek on 22.03.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalculatorServiceTest {

    @Mock
    CountryParamsProvider countryParamsProvider;

    @Mock
    ExchangeRateProvider exchangeRateProvider;

    CalculatorService calculatorService;

    @Before
    public void setup() {
        this.calculatorService = new CalculatorService(countryParamsProvider, exchangeRateProvider);
    }

    @Test
    public void shouldReturnResponseFromAllowedCountries() throws Exception {
        List<CountryParams> allowedCountries = Arrays.asList(new CountryParams("San Escobar", "SE", Currency.getInstance("PLN"), BigDecimal.ZERO, BigDecimal.ONE));
        given(this.countryParamsProvider.allowedCountries()).willReturn(allowedCountries);
        assertTrue(calculatorService.allowedCountries().allowedCountries.get(0).code.equals("SE"));
        assertTrue(calculatorService.allowedCountries().allowedCountries.get(0).name.equals("San Escobar"));
        assertTrue(calculatorService.allowedCountries().allowedCountries.get(0).currencyCode.equals("PLN"));
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionOnWrongNumber() throws Exception {
        calculatorService.calculate("PL", "xxx");

    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionOnNullNumber() throws Exception {
        calculatorService.calculate("PL", null);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldThrowExceptionOnNegativeNumber() throws Exception {
        calculatorService.calculate("PL", "-1");
    }

    @Test(expected = UnknownCountryException.class)
    public void shouldThrowExceptionOnUncnownCountry() throws Exception {
        given(this.countryParamsProvider.getParamsForCountryCode("XX")).willThrow(new UnknownCountryException("XX"));
        calculatorService.calculate("XX", "100");

    }

    @Test
    public void shouldReturnSameValueForGivenParams() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ONE));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, BigDecimal.ONE, new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.00");
        assertEquals("100.00", result.amount);
    }

    @Test
    public void shouldReturn4timesInput() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, BigDecimal.ZERO, BigDecimal.ZERO, new BigDecimal("2")));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, new BigDecimal("2.0000"), new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.00");
        assertEquals("400.00", result.amount);
    }

    @Test
    public void shouldReturn3timesInputMinusCost() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, new BigDecimal("50.51"), BigDecimal.ZERO, new BigDecimal("3")));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, new BigDecimal("1.0000"), new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.00");
        assertEquals("249.49", result.amount);
    }

    @Test
    public void shouldReturn4timesInputTaxed() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, BigDecimal.ZERO, new BigDecimal("0.20"), new BigDecimal("2")));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, new BigDecimal("2.0000"), new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.00");
        assertEquals("320.00", result.amount);
    }

    @Test
    public void shouldReturnAfterCostAndTaxed() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, new BigDecimal("100.00"), new BigDecimal("0.50"), new BigDecimal("10")));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, new BigDecimal("3.0000"), new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.00");
        assertEquals("1350.00", result.amount);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldDenyInputWithScaleBiggerThanTwo() throws Exception {
        Currency currencyToUse = Currency.getInstance("PLN");
        given(this.countryParamsProvider.getParamsForCountryCode("PL")).willReturn(new CountryParams("Polska", "PL", currencyToUse, new BigDecimal("100.00"), new BigDecimal("0.50"), new BigDecimal("10")));
        given(this.exchangeRateProvider.getExchangeRate(currencyToUse)).willReturn(new CurrencyExchangeRate(currencyToUse, new BigDecimal("3.0000"), new Date()));
        CalculationResponse result = calculatorService.calculate("PL", "100.0099");
    }
}