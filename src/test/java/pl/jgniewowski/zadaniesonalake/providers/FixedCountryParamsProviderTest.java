package pl.jgniewowski.zadaniesonalake.providers;

import org.junit.Test;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;
import pl.jgniewowski.zadaniesonalake.model.CountryParams;

import static org.junit.Assert.*;

/**
 * Created by jarek on 22.03.17.
 */
public class FixedCountryParamsProviderTest {

    CountryParamsProvider countryParamsProvider = new FixedCountryParamsProvider();

    @Test(expected = UnknownCountryException.class)
    public void shouldThrowExceptionOnUnknownCountry() throws Exception {
        countryParamsProvider.getParamsForCountryCode("QQ");
    }

    @Test
    public void shouldReturnParamsForPL() throws Exception {
        CountryParams params = countryParamsProvider.getParamsForCountryCode("PL");
        assertNotNull(params);
    }
}