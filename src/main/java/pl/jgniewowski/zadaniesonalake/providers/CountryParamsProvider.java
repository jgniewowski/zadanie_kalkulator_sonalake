package pl.jgniewowski.zadaniesonalake.providers;

import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;
import pl.jgniewowski.zadaniesonalake.model.CountryParams;

import java.util.List;

/**
 * Created by jarek on 22.03.17.
 */
public interface CountryParamsProvider {
    CountryParams getParamsForCountryCode(String countryCode) throws UnknownCountryException;

    List<String> allowedCountryCodes();

    List<CountryParams> allowedCountries();

}
