package pl.jgniewowski.zadaniesonalake.providers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.jgniewowski.zadaniesonalake.exceptions.NoRateDataException;
import pl.jgniewowski.zadaniesonalake.model.CurrencyExchangeRate;
import pl.jgniewowski.zadaniesonalake.providers.dto.ExchangeRate;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

/**
 * Created by jarek on 22.03.17.
 */
@Repository
public class NBPExchangeRateProvider implements ExchangeRateProvider {
    private static final Logger log = LoggerFactory.getLogger(NBPExchangeRateProvider.class);

    @Value("${nbp.api.baseurl}")
    private String baseUrl;

    private String formatQuery = "format=json";

    private RestTemplate restTemplate = new RestTemplate();

    private Currency baseCurrency = Currency.getInstance("PLN");

    public CurrencyExchangeRate getExchangeRate(Currency currency) throws NoRateDataException {
        if(this.isBaseCurrency(currency)) {
            return new CurrencyExchangeRate(baseCurrency, new BigDecimal("1.0000"), new Date());
        }
        String urlSuffix = "rates/a/" + currency.getCurrencyCode().toLowerCase() + "/";
        try {
            ExchangeRate exchangeRate = rateCall(baseUrl, urlSuffix, formatQuery);
            return new CurrencyExchangeRate(currency, new BigDecimal(exchangeRate.rates.get(0).mid), exchangeRate.rates.get(0).effectiveDate);
        } catch (HttpClientErrorException e) {
            throw new NoRateDataException(e);
        }
    }

    private boolean isBaseCurrency(Currency currency) {
        return currency.equals(baseCurrency);
    }

    private ExchangeRate rateCall(String baseUrl, String urlSuffix, String query) throws HttpClientErrorException {
        return restTemplate.getForObject(baseUrl + urlSuffix + "?" + query, ExchangeRate.class);
    }
}
