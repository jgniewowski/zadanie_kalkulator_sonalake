package pl.jgniewowski.zadaniesonalake.providers.dto;

import java.util.Date;

/**
 * Created by jarek on 22.03.17.
 */
public class RateData {

    public String no;
    public Date effectiveDate;
    public String mid;

    @Override
    public String toString() {
        return "RateData{" +
                "no='" + no + '\'' +
                ", effectiveDate=" + effectiveDate +
                ", mid=" + mid +
                '}';
    }
}
