package pl.jgniewowski.zadaniesonalake.providers.dto;

import java.util.List;

/**
 * Created by jarek on 22.03.17.
 */
public class ExchangeRate {
    public String table;
    public String currency;
    public String code;
    public List<RateData> rates;

    @Override
    public String toString() {
        return "CurrencyExchangeRate{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates='" + rates + '\'' +
                '}';
    }
}
