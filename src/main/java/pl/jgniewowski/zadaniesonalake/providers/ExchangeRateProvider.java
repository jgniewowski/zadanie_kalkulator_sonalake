package pl.jgniewowski.zadaniesonalake.providers;

import pl.jgniewowski.zadaniesonalake.exceptions.NoRateDataException;
import pl.jgniewowski.zadaniesonalake.model.CurrencyExchangeRate;

import java.util.Currency;

/**
 * Created by jarek on 22.03.17.
 */
public interface ExchangeRateProvider {
    CurrencyExchangeRate getExchangeRate(Currency currency) throws NoRateDataException;
}
