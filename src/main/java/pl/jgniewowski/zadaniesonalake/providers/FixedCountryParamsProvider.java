package pl.jgniewowski.zadaniesonalake.providers;

import org.springframework.stereotype.Repository;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;
import pl.jgniewowski.zadaniesonalake.model.CountryParams;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by jarek on 22.03.17.
 */
@Repository
public class FixedCountryParamsProvider implements CountryParamsProvider {
    private Map<String, CountryParams> countryParams;

    public FixedCountryParamsProvider() {
        this.countryParams = new HashMap<>();
        countryParams.put("PL", new CountryParams("Polska", "PL", Currency.getInstance("PLN"), new BigDecimal("1200.00"), new BigDecimal("0.19")));
        countryParams.put("DE", new CountryParams("Niemcy", "DE", Currency.getInstance("EUR"), new BigDecimal("800.00"), new BigDecimal("0.20")));;
        countryParams.put("UK", new CountryParams("Wielka Brytania", "UK", Currency.getInstance("GBP"), new BigDecimal("600.00"), new BigDecimal("0.25")));
    }

    @Override
    public List<String> allowedCountryCodes() {
        return new ArrayList<>(countryParams.keySet());
    }

    @Override
    public List<CountryParams> allowedCountries() {
        return new ArrayList<>(countryParams.values());
    }

    @Override
    public CountryParams getParamsForCountryCode(String countryCode) throws UnknownCountryException {
        CountryParams params = countryParams.get(countryCode);
        if(params != null) {
            return params;
        } else {
            throw new UnknownCountryException(countryCode);
        }
    }
}
