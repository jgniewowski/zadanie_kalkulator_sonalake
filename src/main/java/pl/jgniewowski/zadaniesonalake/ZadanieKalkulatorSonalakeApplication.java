package pl.jgniewowski.zadaniesonalake;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZadanieKalkulatorSonalakeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZadanieKalkulatorSonalakeApplication.class, args);
	}
}
