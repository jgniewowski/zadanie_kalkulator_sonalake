package pl.jgniewowski.zadaniesonalake.model;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by jarek on 22.03.17.
 */
public class CountryParams {

    private String countryName;
    private String countryCode;
    private Currency currency;
    private BigDecimal fixedCosts;
    private BigDecimal taxRate;
    private BigDecimal workingDays = BigDecimal.valueOf(22l);


    public CountryParams(String countryName, String countryCode, Currency currency, BigDecimal fixedCosts, BigDecimal taxRate) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.currency = currency;
        this.fixedCosts = fixedCosts;
        this.taxRate = taxRate;
    }

    public CountryParams(String countryName, String countryCode, Currency currency, BigDecimal fixedCosts, BigDecimal taxRate, BigDecimal workingDays) {
        this(countryName, countryCode, currency, fixedCosts, taxRate);
        this.workingDays = workingDays;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getFixedCosts() {
        return fixedCosts;
    }

    public BigDecimal getTaxRate() {
        return taxRate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public BigDecimal getWorkingDays() {
        return workingDays;
    }

    public String getCountryName() {
        return countryName;
    }
}
