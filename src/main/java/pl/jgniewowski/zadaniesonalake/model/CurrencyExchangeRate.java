package pl.jgniewowski.zadaniesonalake.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

/**
 * Created by jarek on 22.03.17.
 */
public class CurrencyExchangeRate {

    private Currency currency;

    private BigDecimal rate;

    private Date rateDate;


    public CurrencyExchangeRate(Currency currency, BigDecimal rate, Date rateDate) {
        this.currency = currency;
        this.rate = rate;
        this.rateDate = rateDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public Date getRateDate() {
        return rateDate;
    }
}
