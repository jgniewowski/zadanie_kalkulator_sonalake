package pl.jgniewowski.zadaniesonalake.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.jgniewowski.zadaniesonalake.controller.dto.CalculateRequest;
import pl.jgniewowski.zadaniesonalake.service.CalculatorService;
import pl.jgniewowski.zadaniesonalake.controller.dto.AllowedCountriesResponse;
import pl.jgniewowski.zadaniesonalake.controller.dto.CalculationResponse;
import pl.jgniewowski.zadaniesonalake.exceptions.NoRateDataException;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;

/**
 * Created by jarek on 22.03.17.
 */
@RestController
public class CalculatorController {
    private static final Logger log = LoggerFactory.getLogger(CalculatorController.class);

    private CalculatorService calculatorService;

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;;
    }

    @RequestMapping(value = "/allowedCountries", method = RequestMethod.GET )
    public AllowedCountriesResponse allowedCurrencies() {
        return calculatorService.allowedCountries();
    }

    @RequestMapping(value = "/calculate", method = RequestMethod.POST )
    public CalculationResponse count(@RequestBody CalculateRequest calculateRequest) throws UnknownCountryException, NoRateDataException {
        log.info("Count called for countryCode: {} and dailyWage: {}", calculateRequest.countryCode, calculateRequest.dailyWage);
        return calculatorService.calculate(calculateRequest.countryCode, calculateRequest.dailyWage);
    }

    @ExceptionHandler(UnknownCountryException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Unknown country.")
    public void handleCurrencyException(UnknownCountryException e) {
        log.error(e.getMessage(), e);
    }

    @ExceptionHandler(NumberFormatException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Wrong amount.")
    public void handleAmountException(NumberFormatException e) {
        log.error(e.getMessage(), e);
    }

    @ExceptionHandler(NoRateDataException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Wrong country.")
    public void handleAmountException(NoRateDataException e) {
        log.error(e.getMessage(), e);
    }
}
