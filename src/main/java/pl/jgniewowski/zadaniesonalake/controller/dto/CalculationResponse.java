package pl.jgniewowski.zadaniesonalake.controller.dto;

/**
 * Created by jarek on 22.03.17.
 */
public class CalculationResponse {

    public String amount;

    public CalculationResponse(String amount) {
        this.amount = amount;
    }

}
