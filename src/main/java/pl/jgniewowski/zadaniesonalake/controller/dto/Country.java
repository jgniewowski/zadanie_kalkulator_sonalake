package pl.jgniewowski.zadaniesonalake.controller.dto;

/**
 * Created by jarek on 23.03.17.
 */
public class Country {

    public String name;
    public String code;
    public String currencyCode;

    public Country(String name, String code, String currencyCode) {
        this.name = name;
        this.code = code;
        this.currencyCode = currencyCode;
    }
}
