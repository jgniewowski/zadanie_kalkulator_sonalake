package pl.jgniewowski.zadaniesonalake.controller.dto;

import java.util.List;

/**
 * Created by jarek on 22.03.17.
 */
public class AllowedCountriesResponse {

    public List<Country> allowedCountries;

    public AllowedCountriesResponse(List<Country> allowedCountries) {
        this.allowedCountries = allowedCountries;
    }
}
