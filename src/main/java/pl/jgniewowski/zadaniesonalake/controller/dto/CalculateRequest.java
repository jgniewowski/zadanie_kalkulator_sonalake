package pl.jgniewowski.zadaniesonalake.controller.dto;

import javax.validation.constraints.NotNull;

/**
 * Created by jarek on 23.03.17.
 */
public class CalculateRequest {

    public String countryCode;
    public String dailyWage;
}
