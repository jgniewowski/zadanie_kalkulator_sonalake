package pl.jgniewowski.zadaniesonalake.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.jgniewowski.zadaniesonalake.controller.dto.AllowedCountriesResponse;
import pl.jgniewowski.zadaniesonalake.controller.dto.CalculationResponse;
import pl.jgniewowski.zadaniesonalake.controller.dto.Country;
import pl.jgniewowski.zadaniesonalake.exceptions.NoRateDataException;
import pl.jgniewowski.zadaniesonalake.exceptions.UnknownCountryException;
import pl.jgniewowski.zadaniesonalake.model.CountryParams;
import pl.jgniewowski.zadaniesonalake.model.CurrencyExchangeRate;
import pl.jgniewowski.zadaniesonalake.providers.CountryParamsProvider;
import pl.jgniewowski.zadaniesonalake.providers.ExchangeRateProvider;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.stream.Collectors;

/**
 * Created by jarek on 22.03.17.
 */
@Service
public class CalculatorService {

    private final DecimalFormat outputFormat = new DecimalFormat("#0.00");

    private CountryParamsProvider countryParamsProvider;

    private ExchangeRateProvider exchangeRateProvider;

    @Autowired
    public CalculatorService(CountryParamsProvider countryParamsProvider, ExchangeRateProvider exchangeRateProvider) {
        this.countryParamsProvider = countryParamsProvider;
        this.exchangeRateProvider = exchangeRateProvider;
    }

    public AllowedCountriesResponse allowedCountries() {
        return new AllowedCountriesResponse(countryParamsProvider.allowedCountries().stream().map(c -> {
            return new Country(c.getCountryName(), c.getCountryCode(), c.getCurrency().getCurrencyCode());
        }).collect(Collectors.toList()));
    }

    public CalculationResponse calculate(String countryCode, String dailyWage) throws NoRateDataException, UnknownCountryException {
        BigDecimal dailyAmount = parseDailyWage(dailyWage);

        CountryParams countryParams = countryParamsProvider.getParamsForCountryCode(countryCode);
        CurrencyExchangeRate currencyExchangeRate = exchangeRateProvider.getExchangeRate(countryParams.getCurrency());

        BigDecimal monthlyWage = dailyAmount.multiply(countryParams.getWorkingDays())
                .subtract(countryParams.getFixedCosts());

        //no taxing when there is no money
        if(monthlyWage.compareTo(BigDecimal.ZERO) > 0) {
            monthlyWage = monthlyWage.multiply(BigDecimal.ONE.subtract(countryParams.getTaxRate()));
        }

        BigDecimal exchangedMonthlyWage = monthlyWage.multiply(currencyExchangeRate.getRate());

        return new CalculationResponse(outputFormat.format(exchangedMonthlyWage.doubleValue()));
    }

    private BigDecimal parseDailyWage(String dailyWage) {
        if(dailyWage == null) {
            throw new NumberFormatException("Daily wage could not be null.");
        }
        BigDecimal dailyAmount = new BigDecimal(dailyWage);
        if(dailyAmount.compareTo(BigDecimal.ZERO) < 0) {
            throw new NumberFormatException("Daily wage should be positive.");
        }
        if(dailyAmount.scale() > 2) {
            throw new NumberFormatException("Scale should be not bigger than 2.");
        }
        return dailyAmount;
    }

}
