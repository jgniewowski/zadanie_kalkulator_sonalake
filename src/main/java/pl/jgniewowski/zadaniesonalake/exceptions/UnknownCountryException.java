package pl.jgniewowski.zadaniesonalake.exceptions;

/**
 * Created by jarek on 22.03.17.
 */
public class UnknownCountryException extends Exception {
    public UnknownCountryException(String s) {
        super("Unknown country " + s);
    }
}
