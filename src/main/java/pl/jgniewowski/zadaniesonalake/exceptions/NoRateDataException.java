package pl.jgniewowski.zadaniesonalake.exceptions;

import org.springframework.web.client.HttpClientErrorException;

/**
 * Created by jarek on 22.03.17.
 */
public class NoRateDataException extends Exception {
    public NoRateDataException(HttpClientErrorException e) {
        super(e);
    }
}
